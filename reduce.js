// problem2.js

const reduce = (elements, cb, startingValue) =>{
    if (!Array.isArray(elements)) {
        throw new Error("Input is not an array");
      }

    if (elements.length === 0 && startingValue === undefined) {
        throw new Error('Cannot reduce an empty array without a starting value.');
    }

    let accumulator = startingValue !== undefined ? startingValue : elements[0];
    const startIndex = startingValue !== undefined ? 0 : 1;

    for (let index = startIndex; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index]);
    }

    return accumulator;
}

module.exports = reduce;
