// test/testProblem1.js

const each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];

try{
each(items,(element,index) => {
    console.log(`Element at index ${index} : ${element}`);
});
}catch(error){
    console.error(error.message);
}