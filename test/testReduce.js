const reduce = require("../reduce.js");

const items = [1, 2, 3, 4, 5, 5];

try {
  const sum = reduce(items, (acc, curr) => acc + curr, 0);
  console.log("Sum of all elements is :", sum);
} catch (error) {
  console.log(error.message);
}
