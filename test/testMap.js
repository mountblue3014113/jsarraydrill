const map = require("../map.js");

const items = [1, 2, 3, 4, 5, 5];

try {
  const double = map(items, (item) => item * 2);
  console.log(double);
} catch (error) {
  console.error(error.message);
}
