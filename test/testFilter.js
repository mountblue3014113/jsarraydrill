const filter = require("../filter.js");

const items = [1, 2, 3, 4, 5, 5];

try {
  const result1 = filter(items, (item) => item > 6);
  console.log(result1);
} catch (error) {
  console.error(error.message);
}
