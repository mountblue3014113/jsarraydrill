const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]];

try {
  console.log(flatten(nestedArray));
} catch (error) {
  console.error("Error caught:", error.message);
}
