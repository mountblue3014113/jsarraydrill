const find = require("../find.js");

const items = [1, 2, 3, 4, 5, 5];

try {
  const result = find(items, (item) => item === 4);
  console.log(result);
} catch (error) {
  console.error(error.message);
}
