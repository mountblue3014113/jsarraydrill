const find = (elements, cb) => {
  if (!Array.isArray(elements)) {
    throw new Error("Input must be an array.");
  }
  if (typeof cb !== "function") {
    throw new Error("Callback must be a function.");
  }

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      return elements[index];
    }
  }
  return undefined;
};

module.exports = find;
