// problem2.js

const flatten = (elements) => {
  if (!Array.isArray(elements)) {
    throw new Error("Invalid input. Please provide an array.");
  }

  const flattenedArray = [];

  const doFlatten = (array) => {
    for (let index = 0; index < array.length; index++) {
      if (Array.isArray(array[index])) {
        doFlatten(array[index]);
      } else {
        flattenedArray.push(array[index]);
      }
    }
  };

  doFlatten(elements);
  return flattenedArray;
};

module.exports = flatten;
