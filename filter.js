const filter = (elements, cb) => {
  if (!Array.isArray(elements)) {
    throw new Error("Element must be an array.");
  }
  if (typeof cb !== "function") {
    throw new Error("cb must be a function.");
  }

  const filteredArray = [];

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      filteredArray.push(elements[index]);
    }
  }

  return filteredArray;
};

module.exports = filter;
