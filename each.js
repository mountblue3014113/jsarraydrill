function each(elements, cb) {
    if(!Array.isArray(elements)){
        throw new Error('Input is not an array !');
    }
    if (typeof cb !== 'function') {
        throw new Error("Callback is not a function");
    }
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
    }
}

module.exports = each;
