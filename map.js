function map(elements, cb) {
  if (!Array.isArray(elements)) {
    throw new Error("Input is not an array");
  }
  if (typeof cb !== "function") {
    throw new Error("Callback is not a function");
  }
  const mappedArray = [];
  for (let index = 0; index < elements.length; index++) {
    mappedArray.push(cb(elements[index]));
  }
  return mappedArray;
}
module.exports = map;
